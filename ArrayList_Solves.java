import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class ArrayList_Solves {
    public static void main(String[] args) {
        // a new array list, add some colors (string) and print out the collection
        ArrayList<String> list = new ArrayList<>();
        list.add("Black");
        list.add("Blue");
        list.add("Red");
        list.add("White");
        list.add("Green");

        for (String string : list) {
            System.out.println(string);
        }

        // iterate through all elements in a array list.
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            it.next();
        }

        // Write a Java program to insert an element into the array list at the first
        // position
        list.set(0, "White");

        // Write a Java program to retrieve an element (at a specified index) from a
        // given array list
        int idx = 0;
        list.get(idx);

        // Write a Java program to update specific array element by given element
        idx = 0;
        String val = "yellow";
        list.set(idx, val);

        // Write a Java program to remove the third element from a array list
        list.remove(2);

        // Write a Java program to search an element in a array list
        list.contains("white");

        // Write a Java program to sort a given array list
        Collections.sort(list);

        // Write a Java program to copy one array list into another
        ArrayList<String> list2 = new ArrayList<>();
        list2.add("ert");
        list2.add("mkl");
        list.addAll(list2);

        // Write a Java program to shuffle elements in a array list
        Collections.shuffle(list2);

        // Write a Java program to reverse elements in a array list.
        Collections.reverse(list2);

        // Write a Java program to extract a portion of a array list
        list.subList(1, 3);

        // Write a Java program to compare two array lists
        list.equals(list2);

        // Write a Java program of swap two elements in an array list
        Collections.swap(list, 0, 4);

        // Write a Java program to join two array lists
        list.addAll(list2);

        // Write a Java program to clone an array list to another array list
        ArrayList<String> list3 = new ArrayList<>(list);

        // Write a Java program to empty an array list.
        list3.clear();

        // Write a Java program to test an array list is empty or not
        list3.isEmpty();

        // Write a Java program to trim the capacity of an array list the current list
        // size
        list2.trimToSize();

        // Write a Java program to increase the size of an array list.
        ArrayList<String> list4 = new ArrayList<>(200);
        list4.ensureCapacity(700);

        // Write a Java program to replace the second element of a ArrayList with the
        // specified element
        list.set(1, "rrrrr");

        // Write a Java program to print all the elements of a ArrayList using the
        // position of the elements
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

    }
}